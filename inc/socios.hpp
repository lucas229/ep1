#ifndef SOCIOS_HPP
#define SOCIOS_HPP

#include "clientes.hpp"

class Socios: public Clientes {
private:
    string dataAssociacao;
public:
    Socios();
    Socios (string nome, string cpf, string telefone, string email, string data);
    Socios (string nome, string cpf, string telefone, string email, vector <string> categorias, vector <int> quantidadesCompradas, string data);
    ~Socios();
    void set_dataAssociacao (string data);
    string get_dataAssociacao ();
    void imprime_dados();
    void save_data();
};

#endif