#ifndef CLIENTES_HPP
#define CLIENTES_HPP

#include <string>
#include <vector>

using namespace std;

class Clientes {
private:
    string nome, cpf, telefone, email;
    int socio;
    struct CategoriasCompradas {
        string nome;
        int quantidade;
    };
    vector <CategoriasCompradas> categoriasCompradas;
public:
    Clientes();
    Clientes (string nome, string cpf, string telefone, string email);
    Clientes (string nome, string cpf, string telefone, string email, vector <string> categoriasCompradas, vector <int> quantidadesCompradas);
    ~Clientes();
    void set_nome(string nome);
    string get_nome();
    void set_cpf (string cpf);
    string get_cpf ();
    void set_telefone (string telefone);
    string get_telefone ();
    void set_email (string email);
    string get_email ();
    void set_socio (int socio);
    int get_socio ();
    virtual void imprime_dados();
    void set_categoriasCompradas (string categoriaComprada, int quantidadeComprada);
    string get_nomeCategoria (int posicao);
    int get_totalCategorias ();
    void update_quantidadeComprada(int posicao, int quantidade);
    int get_quantidadeComprada (int posicao);
    virtual void save_data();
};

#endif