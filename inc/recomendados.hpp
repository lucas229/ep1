#ifndef RECOMENDADOS_HPP
#define RECOMENDADOS_HPP

#include <string>

using namespace std;

class Recomendados {
private:
    string nome;
    int quantidade;
public:
    Recomendados();
    Recomendados (string nome, int quantidade);
    ~Recomendados();
    void set_nome(string nome);
    string get_nome();
    void set_quantidade(int quantidade);
    int get_quantidade();
    void imprime_dados();
};

#endif