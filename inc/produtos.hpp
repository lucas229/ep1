#ifndef PRODUTOS_HPP
#define PRODUTOS_HPP

#include <string>
#include <vector>

using namespace std;

class Produtos {
private:
    string nome;
    double valor;
    int quantidade;
    vector <string> categorias;
public:
    Produtos();
    ~Produtos();
    Produtos(string nome, double valor, int quantidade, vector <string> categorias);
    void set_nome (string nome);
    string get_nome ();
    void set_valor (double valor);
    double get_valor ();
    void set_quantidade (int quantidade);
    int get_quantidade ();
    void set_categorias (vector <string> categorias);
    vector <string> get_categorias ();
    void imprime_dados();
    void save_data ();
};

#endif