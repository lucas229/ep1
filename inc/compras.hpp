#ifndef COMPRAS_HPP
#define COMPRAS_HPP

#include <vector>
#include <string>

using namespace std;

class Compras {
private:
    vector <string> nomes;
    vector <int> quantidades;
    vector <double> valores;
    double total;

public:
    Compras();
    ~Compras();
    void set_nome (string nome);
    string get_nome (int posicao);
    void set_quantidade (int quantidade);
    int get_quantidade (int posicao);
    void set_valor (double valor);
    double get_valor (int posicao);
    void imprime_dados(int socio);
    void limpa_compra ();
    int get_tamanho();
    void update_quantidade (int posicao, int quantidade);
};

#endif