# EP1 - OO 2019.2 (UnB - Gama)

**Lucas Rodrigues Fonseca – 18/0114077**

# Instruções

Para navegar pelo sistema é necessário digitar o número associado à opção que
se deseja utilizar.

## (1) Modo Venda

**(1) Cadastrar novo cliente** – Aqui é possível cadastrar um cliente. Ao final será
perguntado se o cliente é sócio ou não.

**(2) Fazer uma venda** – Nessa opção é possível realizar uma venda. Deverá ser
informado o CPF do cliente. Em seguida podem ser adicionados ao carrinho quantos
produtos forem desejados, informando o nome e a quantidade desse produto.

Existem três formas de listar os clientes cadastrados no sistema:

**(3) Mostrar clientes cadastrados** – Nessa opção serão mostrados todos os clientes e
suas informações, sejam eles sócios ou não.

**(4) Mostrar apenas sócios** – Aqui serão mostrados apenas os clientes cadastrados como
sócios e suas informações.

**(5) Mostrar apenas não sócios** – Aqui serão mostrados apenas os clientes cadastrados
como não sócios e suas informações.

## (2) Modo Estoque

**(1) Cadastrar novo produto** – Nessa opção é possível registrar um novo produto no
sistema. Deverá ser informado o nome do produto, o valor, a quantidade em estoque e as
categorias às quais ele pertence.

**(2) Atualizar estoque** – Aqui deve ser informado o nome do produto e em quanto
aumentar a quantidade desse produto.

**(3) Mostrar produtos cadastrados** – Aqui serão listados todos os produtos cadastrados
no sistema e suas informações.

## (3) Modo Recomendação
Após ser informado o CPF do cliente serão mostrados os 10 produtos cadastrados
no sistema que provavelmente serão de maior interesse do cliente.

Para cada categoria em comum entre aquelas que o cliente já comprou e um
determinado produto, o grau de recomendação desse produto será aumentado o tanto de
vezes que o cliente já comprou desta categoria. Serão mostrados os produtos com maior
grau de recomendação.

## Informações adicionais
* Enviando algum dígito que não esteja associado à nenhuma opção no menu atual
é possível retornar ao menu anterior.
* Não é possível cadastrar CPF, email ou telefone igual para clientes diferentes.
* Não é possível cadastrar nomes iguais para produtos diferentes.

## Bibliotecas utilizadas
iostream, vector, fstream, algorithm, iomanip, ctime, sstream, string