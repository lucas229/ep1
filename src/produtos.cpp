#include "produtos.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>

Produtos::Produtos() {
    nome="";
    valor=0;
    quantidade=0;
}

Produtos::~Produtos() {
}

Produtos::Produtos(string nome, double valor, int quantidade, vector <string> categorias) {
    this->nome=nome;
    this->valor=valor;
    this->quantidade=quantidade;
    this->categorias=categorias;
}

void Produtos::set_nome (string nome) {
    this->nome=nome;
}

string Produtos::get_nome () {
    return nome;
}

void Produtos::set_valor (double valor) {
    this->valor=valor;
}

double Produtos::get_valor () {
    return valor;
}

void Produtos::set_quantidade (int quantidade) {
    this->quantidade=quantidade;
}

int Produtos::get_quantidade () {
    return quantidade;
}

void Produtos::set_categorias (vector <string> categorias) {
    this->categorias=categorias;
}

vector <string> Produtos::get_categorias () {
    return categorias;
}

void Produtos::imprime_dados() {
    cout << setprecision(2) << fixed;
    cout << "Nome: " << nome << endl;
    cout << "Valor: R$" << valor << endl;
    cout << "Quantidade: " << quantidade << endl;
    for (unsigned int i=1; i<=categorias.size(); i++) 
        cout << "Categoria " << i << ": " << categorias[i-1] << endl;
    cout << endl;
}

void Produtos::save_data () {
    ofstream toFile;
    toFile.open("data/produtos.txt", ios::out | ios::app);
    toFile << nome << endl;
    toFile << valor << endl;
    toFile << quantidade << endl;
    toFile << categorias.size() << endl;
    for (string s: categorias)
        toFile << s << endl;
    toFile.close();
}
