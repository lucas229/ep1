#include "recomendados.hpp"

Recomendados::Recomendados() {
    nome="";
    quantidade=0;
}

Recomendados::Recomendados (string nome, int quantidade) {
    this->nome=nome;
    this->quantidade=quantidade;
}

Recomendados::~Recomendados() {
}

void Recomendados::set_nome(string nome) {
    this->nome=nome;
}

string Recomendados::get_nome() {
    return nome;
}

void Recomendados::set_quantidade(int quantidade) {
    this->quantidade=quantidade;
}

int Recomendados::get_quantidade() {
    return quantidade;
}