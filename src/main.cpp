#include "produtos.hpp"
#include "clientes.hpp"
#include "socios.hpp"
#include "compras.hpp"
#include "recomendados.hpp"

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <sstream>

using namespace std;

//Gerais
string getNumberLine ();
string getString ();
template <typename T>
T getInput(T valorMinimo);
int procuraPosicaoProduto (vector <Produtos*> &produtos, string nome);
int procuraPosicaoCliente (vector <Clientes*> &clientes, string cpf);
void carregaProdutos (vector <Produtos*> &produtos);
void carregaClientes (vector <Clientes*> &clientes);
string getDate ();

//Modo venda
int procuraTelefone (vector <Clientes*> &clientes, string telefone);
int procuraEmail (vector <Clientes*> &clientes, string email);
void validaCompras (vector <Produtos*> &produtos, Compras &compras);
int validaQuantidadeComprada(Compras &compras, vector <Produtos*> &produtos);
void atualizaCategoriasCompradas (Compras &compras, vector <Produtos*> &produtos, Clientes* &cliente);
void atualizaQtdeProduto (Compras &compras, vector <Produtos*> &produtos);
void atualizaArquivoClientes (vector <Clientes*> &clientes);
void imprimeClientes (vector <Clientes*> &clientes);
void imprimeSocios (vector <Clientes*> &clientes);
void imprimeNaoSocios (vector <Clientes*> &clientes);

//Modo estoque
vector <string> validaCategorias (int qtdeCategorias);
void imprimeProdutos (vector <Produtos*> &produtos);
void atualizaArquivoProdutos (vector <Produtos*> &produtos);

//Modo Recomendação
int calculaGrauRecomendacao (vector <Recomendados> &recomendados, vector <Produtos*> &produtos, Clientes* &cliente);
void ordenaRecomendados (vector <Recomendados> &recomendados);
void imprimeRecomendados (vector <Recomendados> &recomendados, int quantidade);

int main () {
    int opcao=-1, quantidade, qtdeCategorias, socio, posicao;
    string nome, cpf, telefone, email;
    double valor;
    vector <string> categorias;
    vector <Produtos*> produtos;
    vector <Clientes*> clientes;
    Compras compras;
    vector <Recomendados> recomendados;
    carregaProdutos(produtos);
    carregaClientes(clientes);
    while (opcao!=0) {
        system("clear");
        cout << "Menu" << endl << endl;
        cout << "(1) Modo venda" << endl;
        cout << "(2) Modo estoque" << endl;
        cout << "(3) Modo recomendação" << endl;
        cout << "(0) Finalizar o programa" << endl;
        opcao=getInput <int> (0);
        system("clear");
        switch (opcao) {
        case 1: //Modo venda
            cout << "Modo venda" << endl << endl;
            cout << "(1) Cadastrar novo cliente" << endl;
            cout << "(2) Fazer uma venda" << endl;
            cout << "(3) Mostrar clientes cadastrados" << endl;
            cout << "(4) Mostrar apenas sócios" << endl;
            cout << "(5) Mostrar apenas não sócios" << endl;
            opcao=getInput <int> (1);
            system ("clear");
            switch (opcao) {
            case 1: //Cadastrar novo cliente
                cout << "Nome: ";
                nome=getString();
                cout << "CPF: ";
                cpf=getNumberLine();
                posicao=procuraPosicaoCliente(clientes, cpf);
                if (posicao!=-1) {
                    cout << "CPF já cadastrado. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                cout << "Telefone: ";
                telefone=getNumberLine();
                if (procuraTelefone(clientes, telefone)==1) {
                    cout << "Telefone já cadastrado. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                cout << "Email: ";
                email=getString();
                if (procuraEmail(clientes, email)==1) {
                    cout << "Email já cadastrado. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                cout << "É sócio? (1-Sim / 2-Não): ";
                socio=getInput <int> (1);
                if (socio==1)
                    clientes.push_back (new Socios(nome, cpf, telefone, email, getDate()));
                else
                    clientes.push_back (new Clientes(nome, cpf, telefone, email));
                clientes.back()->save_data();
                break;
            case 2: //Fazer uma venda
                if (produtos.empty()) {
                    cout << "Não foram cadastrados produtos. Entre algum número para continuar. ";
                    opcao=getInput <int> (0);
                    break;
                }
                cout << "CPF do cliente: ";
                cpf=getNumberLine();
                posicao=procuraPosicaoCliente(clientes, cpf);
                if (posicao==-1) {
                    cout << "O cliente deve ser cadastrado antes de realizar a venda. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                validaCompras (produtos, compras);
                if (validaQuantidadeComprada(compras, produtos)==0) {
                    compras.imprime_dados(clientes[posicao]->get_socio());
                    cout << "Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                } else {
                    system ("clear");
                    cout << "Compra cancelada: estoque insuficiente." << endl << "Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                atualizaCategoriasCompradas(compras, produtos, clientes[posicao]);
                atualizaQtdeProduto(compras, produtos);
                atualizaArquivoClientes(clientes);
                atualizaArquivoProdutos(produtos);
                break;
            case 3: //Mostrar clientes cadastrados
                imprimeClientes(clientes);
                cout << "Entre algum número para continuar. ";
                opcao=getInput <int> (1);
                break;
            case 4: //Mostrar apenas sócios
                imprimeSocios(clientes);
                cout << "Entre algum número para continuar. ";
                opcao=getInput <int> (1);
                break;
            case 5: //Mostrar apenas não sócios
                imprimeNaoSocios(clientes);
                cout << "Entre algum número para continuar. ";
                opcao=getInput <int> (1);
            }
            break;
        case 2: //Modo estoque
            cout << "Modo estoque" << endl << endl;
            cout << "(1) Cadastrar novo produto" << endl;
            cout << "(2) Atualizar quantidade" << endl;
            cout << "(3) Mostrar produtos cadastrados" << endl;
            opcao=getInput <int> (1);
            system("clear");
            switch (opcao) {
            case 1: //Cadastrar novo produto
                cout << "Nome: ";
                nome=getString();
                if (procuraPosicaoProduto(produtos, nome)!=-1) {
                    cout << "Produto já cadastrado. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                cout << "Preço: ";
                valor=getInput <double> (0.01);
                cout << "Quantidade: ";
                quantidade=getInput <int> (0);
                cout << "Quantidade de categorias: ";
                qtdeCategorias=getInput <int> (1);
                categorias=validaCategorias(qtdeCategorias);
                produtos.push_back(new Produtos(nome, valor, quantidade, categorias));
                produtos.back()->save_data();
                break;
            case 2: //Atualizar quantidade
                cout << "Nome do produto: ";
                nome=getString();
                posicao=procuraPosicaoProduto(produtos, nome);
                if (posicao==-1) {
                    cout << "Produto não encontrado. Entre algum número para continuar. ";
                    opcao=getInput <int> (1);
                    break;
                }
                cout << "Quantidade a aumentar: ";
                quantidade=getInput <int> (1);
                produtos[posicao]->set_quantidade(produtos[posicao]->get_quantidade()+quantidade);
                atualizaArquivoProdutos(produtos);
                break;
            case 3: //Mostrar produtos cadastrados
                imprimeProdutos(produtos);
                cout << "Entre algum número para continuar. ";
                opcao=getInput <int> (1);
            }
            break;
        case 3: //Modo recomendação
            if (produtos.size()==0) {
                cout << "Não foram cadastrados produtos. Entre algum número para continuar. ";
                opcao=getInput <int> (1);
                break;
            }
            cout << "Modo recomendação" << endl << endl;
            cout << "CPF do cliente: ";
            cpf=getNumberLine();
            posicao=procuraPosicaoCliente(clientes, cpf);
            if (posicao==-1) {
                cout << "Cliente não encontrado. Entre algum número para continuar. ";
                opcao=getInput <int> (1);
                break;
            }
            quantidade=calculaGrauRecomendacao(recomendados, produtos, clientes[posicao]);
            ordenaRecomendados(recomendados);
            imprimeRecomendados(recomendados, quantidade);
            cout << endl << "Entre algum número para continuar. ";
            opcao=getInput <int> (1);
        }
    }
    return 0;
}

//Recebe os produtos recomendados, todos os produtos e um cliente.
//Calcula o grau de recomendação de cada produto para o cliente.
//Retorna a quantidade de produtos a serem apresentados.
int calculaGrauRecomendacao (vector <Recomendados> &recomendados, vector <Produtos*> &produtos, Clientes* &cliente) {
    recomendados.clear();
    int quantidade=0;
    for (Produtos *p: produtos) {
        if (quantidade<10)
            quantidade++;
        int peso=0;
        for (string s: p->get_categorias()) {
            for (int j=0; j<cliente->get_totalCategorias(); j++) {
                if (s==cliente->get_nomeCategoria(j))
                    peso+=cliente->get_quantidadeComprada(j);
            }
        }
        recomendados.push_back(Recomendados(p->get_nome(), peso));
    }
    return quantidade;
}

//Recebe os produtos recomendados.
//Coloca o vetor em ordem alfabética e em seguida em ordem decrescente por grau de recomendação.
void ordenaRecomendados (vector <Recomendados> &recomendados) {
    sort (recomendados.begin(), recomendados.end(), [](Recomendados  a, Recomendados b) { return a.get_nome() < b.get_nome(); });
    sort (recomendados.begin(), recomendados.end(), [](Recomendados  a, Recomendados b) { return a.get_quantidade() > b.get_quantidade(); });
}

//Recebe os produtos recomendados.
//Imprime os produtos recomendados.
void imprimeRecomendados (vector <Recomendados> &recomendados, int quantidade) {
    cout << endl;
    unsigned int maiorNome=recomendados[0].get_nome().size();
    for (int i=1; i<quantidade; i++) {
        if (maiorNome<recomendados[i].get_nome().size())
            maiorNome=recomendados[i].get_nome().size();
    }
    cout << setw(maiorNome+10) << left << "Nome" << "Grau de recomendação" << endl << endl;
    for (int i=0; i<quantidade; i++)
        cout << setw(maiorNome+10) << left << recomendados[i].get_nome() << recomendados[i].get_quantidade()<< endl;
}

//Recebe a quantidade de categorias a serem lidas.
//Retorna as categorias lidas e validadas.
vector <string> validaCategorias (int qtdeCategorias) {
    vector <string> categorias;
    for (int i=1; i<=qtdeCategorias; i++) {
        int achei=0;
        cout << "Categoria " << i << ": ";
        string categoria=getString();
        for (string c: categorias) {
            if (c==categoria) {
                achei=1;
                break;
            }
        }
        if (achei==1) {
            cout << "Categoria já informada. Informe outra: " << endl;
            i--;
            continue;
        }
        categorias.push_back(categoria);
    }
    return categorias;
}

//Recebe os produtos.
//Imprime todos os produtos.
void imprimeProdutos (vector <Produtos*> &produtos) {
    cout << "Produtos" << endl << endl;
    for (Produtos *p: produtos)
        p->imprime_dados();
}

//Recebe os clientes.
//Imprime todos os clientes.
void imprimeClientes (vector <Clientes*> &clientes) {
    cout << "Clientes" << endl << endl;
    for (Clientes *c: clientes)
        c->imprime_dados();
}

//Recebe os clientes.
//Imprime apenas os sócios.
void imprimeSocios (vector <Clientes*> &clientes) {
    cout << "Sócios" << endl << endl;
    for (Clientes *c: clientes) {
        if (c->get_socio()==1)
            c->imprime_dados();
    }
}

//Recebe os clientes.
//Imprime apenas os não sócios.
void imprimeNaoSocios (vector <Clientes*> &clientes) {
    cout << "Não sócios" << endl << endl;
    for (Clientes *c: clientes) {
        if (c->get_socio()!=1)
            c->imprime_dados();
    }
}

//Recebe os clientes.
//Reescreve o arquivo de clientes com os novos dados.
void atualizaArquivoClientes (vector <Clientes*> &clientes) {
    remove ("data/clientes.txt");
    for (Clientes *c: clientes)
        c->save_data();
}

//Recebe os clientes e um telefone.
//Retorna 1 caso o telefone seja encontrado ou 0 caso contrário.
int procuraTelefone (vector <Clientes*> &clientes, string telefone) {
    for (Clientes *c: clientes) {
        if (telefone==c->get_telefone())
            return 1;
    }
    return 0;
}

//Recebe os clientes e um email.
//Retorna 1 caso o email seja encontrado ou 0 caso contrário.
int procuraEmail (vector <Clientes*> &clientes, string email) {
    for (Clientes *c: clientes) {
        if (email==c->get_email())
            return 1;
    }
    return 0;
}

//Retorna a data do sistema.
string getDate () {
    auto t=time(nullptr);
    auto tm=*localtime(&t);
    ostringstream oss;
    oss << put_time(&tm, "%d/%m/%Y");
    return oss.str();
}

//Recebe os produtos e as compras
//Lê e adiciona as compras ao carrinho
void validaCompras (vector <Produtos*> &produtos, Compras &compras) {
    compras.limpa_compra();
    int opcao=1;
    while (opcao==1) {
        cout << "Nome do produto: ";
        string nome=getString();
        int posicaoProduto=procuraPosicaoProduto(produtos, nome);
        if (posicaoProduto==-1) {
            cout << "Produto não encontrado." << endl;
            continue;
        }
        compras.set_valor(produtos[posicaoProduto]->get_valor());
        cout << "Quantidade: ";
        int quantidade=getInput <int> (1);
        int achei=0;
        for (int i=0; i<compras.get_tamanho(); i++) {
            if (nome==compras.get_nome(i)) {
                compras.update_quantidade(i, compras.get_quantidade(i)+quantidade);
                achei=1;
                break;
            }
        }
        if (achei==0) {
            compras.set_nome(nome);
            compras.set_quantidade(quantidade);
        }
        cout << "Adicionar mais um produto (1-Sim, 2-Não): ";
        opcao=getInput <int> (1);
    }
}

//Recebe as compras e os produtos.
//Retorna 1 caso a quantidade pedida de um produto seja maior do que a disponível ou 0 caso contrário.
int validaQuantidadeComprada(Compras &compras, vector <Produtos*> &produtos) {
    for (int i=0; i<compras.get_tamanho(); i++) {
        int posicaoProduto=procuraPosicaoProduto(produtos, compras.get_nome(i));
        if (compras.get_quantidade(i)>produtos[posicaoProduto]->get_quantidade()) {
            return 1;
            break;
        }
    }
    return 0;
}

//Recebe a compra, os produtos e um cliente.
//Salva as categorias dos produtos comprados no histórico do cliente.
void atualizaCategoriasCompradas (Compras &compras, vector <Produtos*> &produtos, Clientes* &cliente) {
    for (int i=0; i<compras.get_tamanho(); i++) {
        int posicaoProduto=procuraPosicaoProduto(produtos, compras.get_nome(i));
        for (string s: produtos[posicaoProduto]->get_categorias()) {
            int achei=0;
            for (int j=0; j<cliente->get_totalCategorias(); j++) {
                if (cliente->get_nomeCategoria(j)==s) {
                    cliente->update_quantidadeComprada(j, compras.get_quantidade(i)+cliente->get_quantidadeComprada(j));
                    achei=1;
                    break;
                }
            }
            if (achei==0)
                cliente->set_categoriasCompradas(s, compras.get_quantidade(i));
        }
    }
}

//Recebe as compras os produtos.
//Dá baixa nos produtos comprados.
void atualizaQtdeProduto (Compras &compras, vector <Produtos*> &produtos) {
    for (int i=0; i<compras.get_tamanho(); i++) {
        for (Produtos *p: produtos) {
            if (compras.get_nome(i)==p->get_nome())
                p->set_quantidade(p->get_quantidade()-compras.get_quantidade(i));
        }
    }
}

//Retorna uma string com apenas números.
string getNumberLine () {
    string valor;
    while (1) {
        getline (cin, valor);
        int achei=0;
        for (char c: valor) {
            if (!isdigit(c)) {
              achei=1;
              break;
            }
        }
        if (achei==0 && !valor.empty())
            return valor;
        cout << "Entrada inválida. Informe novamente: ";
    }
}

//Retorna uma string que não esteja vazia ou somente com espaços.
string getString () {
    string valor;
    while (1) {
        getline (cin, valor);
        unsigned int achei=0;
        for (char c: valor) {
            if (c==' ' || c=='	')
                achei++;
        }
        if (achei!=valor.size()) {
            return valor;
        }
        cout << "Entrada inválida. Informe novamente: ";
    }
    return valor;
}

//Recebe um valor mínimo para ser lido.
//Retorna um número maior ou igual ao valor mínimo.
template <typename T>
T getInput(T valorMinimo) {
    T valor;
    while (1) {
        cin >> valor;
        if (cin.fail() || valor<valorMinimo) {
            cin.clear();
            cin.ignore(32767, '\n');
            cout << "Entrada inválida. Informe novamente: ";
        }
        else {
            cin.ignore (32767, '\n');
            return valor;
        }
    }
}

//Recebe os produtos e um nome.
//Retorna a posicão do cliente com o nome especificado ou -1 caso não seja encontrado.
int procuraPosicaoProduto (vector <Produtos*> &produtos, string nome) {
    for (unsigned int i=0; i<produtos.size(); i++) {
        if (produtos[i]->get_nome()==nome)
            return i;
    }
    return -1;
}

//Recebe os clientes e um CPF.
//Retorna a posição do cliente com o CPF especificado ou -1 caso não seja encontrado.
int procuraPosicaoCliente (vector <Clientes*> &clientes, string cpf) {
    for (unsigned int i=0; i<clientes.size(); i++) {
        if (clientes[i]->get_cpf()==cpf)
            return i;
    }
    return -1;
}

//Recebe os produtos.
//Carrega os dados dos produtos salvos no arquivo.
void carregaProdutos (vector <Produtos*> &produtos) {
    ifstream inFile;
    string nome, categoria;
    double valor;
    int quantidade, qtdeCategorias;
    vector <string> categorias;
    inFile.open("data/produtos.txt");
    while (getline (inFile, nome)) {
        inFile >> valor;
        inFile >> quantidade;
        inFile >> qtdeCategorias;
        inFile.ignore();
        categorias.clear();
        for (int i=0; i<qtdeCategorias; i++) {
            getline (inFile, categoria);
            categorias.push_back(categoria);
        }
        produtos.push_back(new Produtos(nome, valor, quantidade, categorias));
    }
    inFile.close();
}

//Recebe os clientes.
//Carrega os dados dos clientes salvos no arquivo.
void carregaClientes (vector <Clientes*> &clientes) {
    ifstream inFile;
    string nome,cpf, telefone, email, categoria, data;
    int socio, qtdeCategorias, quantidade;
    vector <string> categorias;
    vector <int> quantidadesCompradas;
    inFile.open("data/clientes.txt");
    while (getline (inFile, nome)) {
        getline (inFile, cpf);
        getline (inFile, telefone);
        getline (inFile, email);
        inFile >> socio;
        inFile >> qtdeCategorias;
        inFile.ignore();
        categorias.clear();
        quantidadesCompradas.clear();
        for (int i=0; i<qtdeCategorias; i++) {
            getline (inFile, categoria);
            inFile >> quantidade;
            inFile.ignore();
            categorias.push_back(categoria);
            quantidadesCompradas.push_back(quantidade);
        }
        if (socio==1) {
            getline (inFile, data);
            clientes.push_back(new Socios (nome, cpf, telefone, email, categorias, quantidadesCompradas, data));
        }
        else
            clientes.push_back(new Clientes (nome, cpf, telefone, email, categorias, quantidadesCompradas));
    }
    inFile.close();
}

//Recebe os produtos.
//Reescreve o arquivo de produtos com os novos dados.
void atualizaArquivoProdutos (vector <Produtos*> &produtos) {
    remove ("data/produtos.txt");
    for (Produtos *p: produtos)
        p->save_data();
}