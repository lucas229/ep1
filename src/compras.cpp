#include "compras.hpp"

#include <iostream>
#include <iomanip>

Compras::Compras() {
    total=0;
}

Compras::~Compras() {
}

void Compras::set_nome (string nome) {
    nomes.push_back(nome);
}

string Compras::get_nome (int posicao) {
    return nomes[posicao];
}

void Compras::set_quantidade (int quantidade) {
    quantidades.push_back(quantidade);
}

int Compras::get_quantidade (int posicao) {
    return quantidades[posicao];
}

void Compras::set_valor (double valor) {
    valores.push_back(valor);
}

double Compras::get_valor (int posicao) {
    return valores[posicao];
}

void Compras::imprime_dados(int socio) {
    system ("clear");
    unsigned int maiorNome=nomes[0].size();
    unsigned int maiorQuantidade=to_string(quantidades[0]).size();
    for (unsigned int i=1; i<nomes.size(); i++) {
        if (maiorNome<nomes[i].size())
            maiorNome=nomes[i].size();
        if (maiorQuantidade<to_string(quantidades[i]).size())
            maiorQuantidade=to_string(quantidades[i]).size();
    }
    cout << setprecision(2) << fixed;
    cout << setw(maiorNome+10) << left << "Nome" << setw(maiorQuantidade+15) << left << "Quantidade" << "Valor" << endl << endl;
    for (unsigned int i=0; i<nomes.size(); i++) {
        cout << setw(maiorNome+10) << left << nomes[i] << setw (maiorQuantidade+15) << left << quantidades[i] << "R$" << valores[i]*quantidades[i] << endl;
        total+=valores[i]*quantidades[i];
    }
    cout << endl << "Total: R$" << total << endl;
    cout << "Desconto para sócio: R$" << total*0.15 << endl;
    cout << "Sócio paga: R$" << total-total*0.15 << endl;
    cout << "Valor final para o cliente: R$";
    if (socio==1)
        cout << total-total*0.15;
    else
        cout << total;
    cout << endl << endl;
}

void Compras::limpa_compra () {
    total=0;
    nomes.clear();
    quantidades.clear();
    valores.clear();
}

int Compras::get_tamanho() {
    return nomes.size();
}

void Compras::update_quantidade (int posicao, int quantidade) {
    quantidades[posicao]=quantidade;
}