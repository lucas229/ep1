#include "socios.hpp"

#include <iostream>
#include <fstream>

Socios::Socios() {
    set_nome("");
    set_cpf("");
    set_telefone("");
    set_email("");
    set_socio(1);
    set_dataAssociacao("");
}

Socios::Socios (string nome, string cpf, string telefone, string email, string data) {
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    set_socio(1);
    this->dataAssociacao=data;
}

Socios::Socios (string nome, string cpf, string telefone, string email, vector <string> categorias, vector <int> quantidadesCompradas, string data) {
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    for (unsigned int i=0; i<categorias.size(); i++)
        set_categoriasCompradas (categorias[i], quantidadesCompradas[i]);
    set_socio(1);
    set_dataAssociacao (data);
}

Socios::~Socios() {
}

void Socios::set_dataAssociacao (string data) {
    this->dataAssociacao=data;
}

string Socios::get_dataAssociacao () {
    return dataAssociacao;
}

void Socios::imprime_dados() {
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;
    cout << "Email: " << get_email() << endl;
    cout << "Sócio: Sim" << endl;
    cout << "Data de associação: " << dataAssociacao << endl << endl;
}

void Socios::save_data () {
    ofstream toFile;
    toFile.open("data/clientes.txt", ios::out | ios::app);
    toFile << get_nome() << endl;
    toFile << get_cpf() << endl;
    toFile << get_telefone() << endl;
    toFile << get_email() << endl;
    toFile << get_socio() << endl;
    toFile << get_totalCategorias() << endl;
    for (int i=0; i<get_totalCategorias(); i++) {
        toFile << get_nomeCategoria(i) << endl;
        toFile << get_quantidadeComprada(i) << endl;
    }
    toFile << dataAssociacao << endl;
    toFile.close();
}