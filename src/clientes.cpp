#include "clientes.hpp"

#include <iostream>
#include <fstream>
#include <algorithm>

Clientes::Clientes() {
    nome="";
    cpf="";
    telefone="";
    email="";
    socio=2;
}

Clientes::~Clientes() {
}

Clientes::Clientes (string nome, string cpf, string telefone, string email) {
    this->nome=nome;
    this->cpf=cpf;
    this->telefone=telefone;
    this->email=email;
    this->socio=2;
}

Clientes::Clientes(string nome, string cpf, string telefone, string email, vector <string> categoriasCompradas, vector <int> quantidadesCompradas) {
    this->nome=nome;
    this->cpf=cpf;
    this->telefone=telefone;
    this->email=email;
    this->socio=2;
    for (unsigned int i=0; i<categoriasCompradas.size(); i++) {
        this->categoriasCompradas.push_back({categoriasCompradas[i], quantidadesCompradas[i]});
    }
}

void Clientes::set_nome(string nome) {
    this->nome=nome;
}

string Clientes::get_nome() {
    return nome;
}

void Clientes::set_cpf (string cpf) {
    this->cpf=cpf;
}

string Clientes::get_cpf () {
    return cpf;
}

void Clientes::set_telefone (string telefone) {
    this->telefone=telefone;
}

string Clientes::get_telefone () {
    return telefone;
}

void Clientes::set_email (string email) {
    this->email=email;
}

string Clientes::get_email () {
    return email;
}

void Clientes::set_socio (int socio) {
    this->socio=socio;
}

int Clientes::get_socio () {
    return socio;
}

void Clientes::imprime_dados() {
    cout << "Nome: " << nome << endl;
    cout << "CPF: " << cpf << endl;
    cout << "Telefone: " << telefone << endl;
    cout << "Email: " << email << endl;
    cout << "Sócio: Não" << endl << endl;
}

void Clientes::set_categoriasCompradas (string categoriaComprada, int quantidadeComprada) {
    this->categoriasCompradas.push_back({categoriaComprada, quantidadeComprada});
}

string Clientes::get_nomeCategoria (int posicao) {
    return categoriasCompradas[posicao].nome;
}

int Clientes::get_totalCategorias () {
    return categoriasCompradas.size();
}

void Clientes::update_quantidadeComprada(int posicao, int quantidade) {
    categoriasCompradas[posicao].quantidade=quantidade;
}

int Clientes::get_quantidadeComprada (int posicao) {
    return categoriasCompradas[posicao].quantidade;
}

void Clientes::save_data () {
    ofstream toFile;
    toFile.open("data/clientes.txt", ios::out | ios::app);
    toFile << nome << endl;
    toFile << cpf << endl;
    toFile << telefone << endl;
    toFile << email << endl;
    toFile << socio << endl;
    toFile << categoriasCompradas.size() << endl;
    for (unsigned int i=0; i<categoriasCompradas.size(); i++) {
        toFile << categoriasCompradas[i].nome << endl;
        toFile << categoriasCompradas[i].quantidade << endl;
    }
    toFile.close();
}